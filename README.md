# XModeAPI

[![CI Status](http://img.shields.io/travis/Gabriel Jensen/XModeAPI.svg?style=flat)](https://travis-ci.org/Gabriel Jensen/XModeAPI)
[![Version](https://img.shields.io/cocoapods/v/XModeAPI.svg?style=flat)](http://cocoapods.org/pods/XModeAPI)
[![License](https://img.shields.io/cocoapods/l/XModeAPI.svg?style=flat)](http://cocoapods.org/pods/XModeAPI)
[![Platform](https://img.shields.io/cocoapods/p/XModeAPI.svg?style=flat)](http://cocoapods.org/pods/XModeAPI)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

XModeAPI is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "XModeAPI"
```

## Author

Gabriel Jensen, gabriel@brainjelly.com

## License

XModeAPI is available under the MIT license. See the LICENSE file for more info.

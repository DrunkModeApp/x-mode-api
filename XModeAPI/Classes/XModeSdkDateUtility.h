//
//  XModeSdkDateUtility.h
//  DrunkModeApp
//
//  Created by Gabriel Jensen on 6/16/16.
//  Copyright © 2016 launch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XModeSdkDateUtility : NSObject

+ (int) hourOfDayNow;

@end

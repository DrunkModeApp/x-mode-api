//
//  XModeSdkDateUtility.m
//  DrunkModeApp
//
//  Created by Gabriel Jensen on 6/16/16.
//  Copyright © 2016 launch. All rights reserved.
//

#import <XModeAPI/XModeSdkDateUtility.h>

@implementation XModeSdkDateUtility

+ (int) hourOfDayNow {
    NSDate *date = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
    NSInteger hour = [components hour];
    return (int) hour;
   // NSInteger minute = [components minute];
}

@end

//
//  NSString+XModeSdk.h
//  DrunkModeApp
//
//  Created by Gabriel Jensen on 11/7/16.
//  Copyright © 2016 launch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (XModeSdk)

- (BOOL)isValidIPAddressV4;
- (BOOL)isValidIPAddressV6;

@end

//
//  XModeSdkUtil.h
//  Pods
//
//  Created by Gabriel Jensen on 12/13/16.
//
//

#import <Foundation/Foundation.h>

@interface XModeSdkUtil : NSObject

typedef enum {
    ConnectionTypeUnknown,
    ConnectionTypeNone,
    ConnectionType3G,
    ConnectionTypeWiFi
} ConnectionType;

+ (NSString *)XgetIPAddress;
+ (NSString *)getIPAddress:(BOOL)preferIPv4;
+ (NSDictionary *)getIPAddresses;
+ (NSString*)encodeToString64:(NSString*)fromString;
+ (ConnectionType)networkConnectionType;

@end

//
//  XModeSdkVisitedPlace.h
//  DrunkModeApp
//
//  Created by Gabriel Jensen on 11/1/14.
//  Copyright (c) 2014 launch. All rights reserved.
//

@import Foundation;
@import MapKit;

/** Represents a geographic spot where the user is known to have been. */
@interface XModeSdkVisitedPlace : NSObject <MKAnnotation>


- (instancetype)initWithLatitude:(double)latitude
                       longitude:(double)longitude
                        altitude:(double)altitude
                horizontalAccuracy:(double)hAccuracy
                  verticalAccuracy:(double)vAccuracy
                            course:(double)course
                             speed:(double)speed
                         timestamp:(NSDate *)timestamp;

- (instancetype) initWithLocation: (CLLocation*) location
                             note: (NSString*) note;

@property (readonly, nonatomic) CLLocation *location;
@property (nonatomic) NSString *note;

@end

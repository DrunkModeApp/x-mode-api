//
//  XModeSdkLocationManager.m
//  DrunkModeApp
//
//  Created by Tony Lenzi / Gabriel Jensen on 7/16/15.
//  Copyright (c) 2015 launch. All rights reserved.
//

#import <XModeAPI/XModeSdkLocationManager.h>
#import <XModeAPI/XModeSdkVisitedPlace.h>
#import <XModeAPI/XModeSdkDateUtility.h>
#import <XModeAPI/XModeAPI.h>

#define MININUM_LOCATION_ACCURACY 200
#define AGE_SECONDS_TO_USE_ANY_ACCURACY 60*60

@interface XModeSdkLocationManager() <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocation *lastKnownLocation;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic) NSArray *locationPermissionDeniedMessages;

@end

@implementation XModeSdkLocationManager

+ (id)sharedInstance {
    static XModeSdkLocationManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id) init {
    if (self = [super init]) {
        self.locationPermissionDeniedMessages = @[@{@"title": @"Location turned off?", @"message":@"We need location turned on for the app to run properly."}];
    }
    return self;
}

#pragma mark - Location Lifecycle

- (void) startLocationGathering {
    
    if (!self.locationManager) {
        self.locationManager = [[CLLocationManager alloc] init];
        
        // Only allowBackgroundLoc if app has it turned on
        NSArray* backgroundModes = [[NSBundle mainBundle].infoDictionary objectForKey:@"UIBackgroundModes"];
        
        if (![[XModeAPI sharedInstance] isLocationInForegroundOnly]) {
            if (backgroundModes && [backgroundModes containsObject:@"location"]) {
                if ([ self.locationManager respondsToSelector:@selector(setAllowsBackgroundLocationUpdates:)]) {
                    self.locationManager.allowsBackgroundLocationUpdates = YES;
                }
            }
        }
        
        self.locationManager.delegate = self;
        self.locationManager.distanceFilter = 15.0;
        self.locationManager.activityType = CLActivityTypeOther; // This is the default included just fyi.
        self.locationManager.pausesLocationUpdatesAutomatically = YES;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    }
    
    [self.locationManager startUpdatingLocation];
    [self.locationManager startMonitoringSignificantLocationChanges];
}

- (void) locationManager:(CLLocationManager *)manager didExitRegion:(nonnull CLRegion *)region {
    [self incrementNumGeoFenceWakeLocal];
    NSLog(@"didExitRegion");
    NSNotification *notif = [NSNotification notificationWithName:XModeGeoFenceDidExit object:nil userInfo:nil];
    NSNotificationQueue *queue = [NSNotificationQueue defaultQueue];
    [queue enqueueNotification:notif postingStyle:NSPostWhenIdle coalesceMask:NSNotificationCoalescingOnName forModes:nil];
    
}

- (void) incrementNumGeoFenceWakeLocal {
    NSInteger num = [[NSUserDefaults standardUserDefaults] integerForKey:@"NumTimesGeoFenceWake"];
    num = num + 1;
    NSLog(@"--NumTimesGeoFenceWake:%ld", (long)num);
    [[NSUserDefaults standardUserDefaults] setInteger:num forKey:@"NumTimesGeoFenceWake"];
}

- (void) stopBackgroundLocation {
    self.locationManager.allowsBackgroundLocationUpdates = NO;
}

- (void) startBackgroundLocation {
    self.locationManager.allowsBackgroundLocationUpdates = YES;
}


#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    self.prevLocation = self.location;
    self.location = [locations lastObject];
    
    if ([[XModeAPI sharedInstance] isDebugMode]){
       NSLog(@"[XModeSdkLocationManager] didUpdateLocations. location timestamp:%f", [self.location.timestamp timeIntervalSince1970]);
    }
    
    if (self.location.horizontalAccuracy < 0) {
        return;
    }
    
    // Create a region around where you are. Must be done here because locationManager does NOT have current loc when inited.
    for (CLRegion *monitored in [self.locationManager monitoredRegions]) {
        [self.locationManager stopMonitoringForRegion:monitored];
    }
    
    CLCircularRegion *region = [[CLCircularRegion alloc] initWithCenter:self.location.coordinate radius:10.0 identifier:@"center"];
    [self.locationManager startMonitoringForRegion:region];
    
    BOOL prevPointSoLongAgoUseAnyAccuracy = NO;
    
    if (self.prevLocation) {
        NSTimeInterval secondsBetweenDates = [self.location.timestamp timeIntervalSinceDate:self.prevLocation.timestamp];
        if (secondsBetweenDates > AGE_SECONDS_TO_USE_ANY_ACCURACY) {
            prevPointSoLongAgoUseAnyAccuracy = YES;
        }
    } else {
        prevPointSoLongAgoUseAnyAccuracy = YES;
    }
    
    if (self.location.horizontalAccuracy > MININUM_LOCATION_ACCURACY || prevPointSoLongAgoUseAnyAccuracy) {
        return;
    }
    
    XModeSdkVisitedPlace *visitedPlace = [[XModeSdkVisitedPlace alloc] initWithLocation:self.location note:@""];
    
    // NSLog(@"[XMode] locationManager: notify location change: %@", visitedPlace);
    
    NSNotification *notif = [NSNotification notificationWithName:XModeCurrentLocationDidChangeNotification object:nil userInfo:@{ XModeCurrentLocationKey: visitedPlace}];
    NSNotificationQueue *queue = [NSNotificationQueue defaultQueue];
    [queue enqueueNotification:notif
                  postingStyle:NSPostWhenIdle
                  coalesceMask:NSNotificationCoalescingOnName
                      forModes:nil];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    NSAssert([NSThread isMainThread], @"Unexpected thread. Add thread marshaling code to move to the main thread.");
    
    if (self.hasCurrentLocationPermission) {
        [self startLocationGathering];
        // [BeaconsInSpace provideAPIKey:@"8468CD02442644BCBF3EA82DF35CC2AB56CF79F80B52B735093700"];
        
    } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        if (![[XModeAPI sharedInstance] isLocationInForegroundOnly]) {
            
            if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]){
                [self.locationManager requestAlwaysAuthorization];
            }
            
        } else {
            if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
                [self.locationManager requestWhenInUseAuthorization];
            }
        }
        
    } else if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        if ([[XModeAPI sharedInstance] isDebugMode]){
            NSLog(@"[GJPGLX] CLLocationManager kCLAuthorizationStatusDenied");
        }
    } else if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted) {
        if ([[XModeAPI sharedInstance] isDebugMode]){
            NSLog(@"[GJPGLX] CLLocationManager kCLAuthorizationStatusRestricted");
        }
    }
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:XModeCurrentLocationPermissionDidChangeNotification
                      object:nil];
}


#pragma mark - Permissions

- (BOOL)hasAskedForCurrentLocationPermission
{
    return [CLLocationManager authorizationStatus] != kCLAuthorizationStatusNotDetermined;
}

- (BOOL)hasBeenDeniedCurrentLocationPermission
{
    return [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied;
}

- (BOOL)hasCurrentLocationPermission
{
    // Determine the authorization status by exclusion, because the CLAuthorizationStatus
    // enum underwent significant changes in iOS 8. To support both iOS 7.1 and iOS 8 we
    // should only reference the enum values available in both versions.
    const CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    return status != kCLAuthorizationStatusNotDetermined &&
    status != kCLAuthorizationStatusDenied        &&
    status != kCLAuthorizationStatusRestricted;
}

-(void) sendLocationDeniedNotification {
    // Check if it's time to send
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [NSDate date];
    
    int rndValue = 0;
    localNotification.userInfo = @{@"type" : @"locationPermissionDenied"};
    localNotification.alertBody = [NSString stringWithFormat:@"%@ %@", self.locationPermissionDeniedMessages[rndValue][@"title"],self.locationPermissionDeniedMessages[rndValue][@"message"]];
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

#pragma mark - Utility

@end

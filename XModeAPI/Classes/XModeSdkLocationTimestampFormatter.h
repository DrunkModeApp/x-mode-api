//
//  XModeSdkLocationTimestampFormatter.h
//  DrunkModeApp
//
//  Created by Gabriel Jensen on 10/8/14.
//  Copyright (c) 2014 launch. All rights reserved.
//

/** Utility class used when creating pin annotations on the FindMyDrunk and Breadcrumbs maps. */
@interface XModeSdkLocationTimestampFormatter : NSObject

+ (instancetype)sharedInstance;

- (NSString *)stringFromTimestamp:(NSDate *)timestamp;

@end

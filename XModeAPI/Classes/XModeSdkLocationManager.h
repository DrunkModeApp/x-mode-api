//
//  XModeSdkLocationManager.h
//  DrunkModeApp
//
//  Created by Tony Lenzi on 7/16/15.
//  Copyright (c) 2015 launch. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreLocation;


/** Posted on the main thread when the app's access to the user's location is granted/revoked. */

@interface XModeSdkLocationManager : NSObject

/** Returns the sole instance of XModeSdkLocationManager used by this app. */
+ (instancetype)sharedInstance;

/** Returns YES if the user has already been asked to grant access to their current location. */
@property (readonly, nonatomic) BOOL hasAskedForCurrentLocationPermission;

/** Returns YES if the user was asked to give this app location permission, but chose to deny the request. */
@property (readonly, nonatomic) BOOL hasBeenDeniedCurrentLocationPermission;

/** Returns YES if the user has granted this app permission to track their location. */
@property (readonly, nonatomic) BOOL hasCurrentLocationPermission;

@property (nonatomic, copy) CLLocation *location;
@property (nonatomic, copy) CLLocation *prevLocation;

- (void)startLocationGathering;
- (void)stopBackgroundLocation;
- (void)startBackgroundLocation;

@end

//
//  XModeAPI.m
//  Pods
//
//  Created by Gabriel Jensen on 11/30/16.
//
//

#include <BeaconsInSpace/BeaconsInSpace.h>
#import <XModeAPI/XModeAPI.h>
#import <XModeAPI/XModeSdkLocationManager.h>
#import <XModeAPI/XModeSdkLocationPublisher.h>
#import <XModeAPI/XModeSdkSettingsManager.h>
#import <AJKit/AJKit.h>

NSString *const XModeCurrentLocationDidChangeNotification           = @"XModeCurrentLocationDidChangeNotification";
NSString *const XModeGeoFenceDidExit                                = @"XModeGeoFenceExited";
NSString *const XModeCurrentLocationDidChangeViewsNotification      = @"XModeCurrentLocationDidChangeViewsNotification";
NSString *const XModeCurrentLocationPermissionDidChangeNotification = @"XModeCurrentLocationPermissionDidChangeNotification";
NSString *const XModeLocationUpdatedOnServerNotification            = @"XModeLocationUpdatedOnServerNotification";
NSString *const XModeCurrentLocationKey                             = @"XModeCurrentLocationKey";

@interface XModeAPI ()

@property (nonatomic) XModeSdkLocationPublisher *publisher;
@property (nonatomic) BOOL didInitAJKit;
@property (nonatomic) BOOL isLocationInUse;
@property (nonatomic) NSString *apiKey;
@property (nonatomic) BOOL clientWantsToUseBeaconsInSpace;

@end

@implementation XModeAPI

+ (id)sharedInstance {
    static XModeAPI *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id) init {
    if (self = [super init]) {
        self.isLocationInUse = YES;
        self.isLocationInForegroundOnly = NO;
        self.didInitAJKit = NO;
        self.recordLatestToUserDefaults = NO;
        self.isWakingForLocation = NO;
        self.isDebugMode = NO;
        self.clientWantsToUseBeaconsInSpace = YES;
        self.debugSuffixToAddToIdfa = @"";
    }
    return self;
}

+ (void) debugLocal: (NSString*) lineToAdd {
    if ([[XModeAPI sharedInstance] isDebugMode]) {
        NSLog(@"%@", lineToAdd);
        if ([[XModeAPI sharedInstance] isWakingForLocation]) {
            NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"XModeDebugLog"];
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@*%@",savedValue,lineToAdd] forKey:@"XModeDebugLog"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
}

- (void)startWithApiKey :(NSString*) apiKey {
    self.apiKey = apiKey;
    [XModeAPI debugLocal:@"Start api"];
    [[XModeSdkLocationManager sharedInstance] startLocationGathering];
    [[XModeSdkLocationPublisher sharedInstance] startPublishingLocationUpdates: apiKey];
    if (!self.isLocationInForegroundOnly && self.clientWantsToUseBeaconsInSpace && [[XModeSdkSettingsManager sharedInstance] useBeaconsInSpace])  {
        if (![self.apiKey isEqualToString:@"2LRjA3UvYC3aSUEVtKo8ClP8gEboiMG9Gqkg404h"]) {
            [BeaconsInSpace provideAPIKey:@"8468CD02442644BCBF3EA82DF35CC2AB56CF79F80B52B735093700"];
        }
    }
    
}

// BeaconsInSpace can be opted out of by the client.
- (void) startWithApiKey :(NSString*) apiKey useBeaconsInSpace:(BOOL) useBeaconsInSpace {
    self.clientWantsToUseBeaconsInSpace = useBeaconsInSpace;
    [self startWithApiKey:apiKey];
}

- (void)startWithApiKey :(NSString*) apiKey isLocationInForegroundOnly: (BOOL) isLocationInForegroundOnly {
    self.isLocationInForegroundOnly = isLocationInForegroundOnly;
    [[XModeAPI sharedInstance] startWithApiKey:apiKey];

}

- (void)startWithApiKeyAndNoLocation :(NSString*) apiKey  {
    self.apiKey = apiKey;
    self.isLocationInUse = NO;
    [[XModeSdkLocationPublisher sharedInstance] startPublishingLocationUpdates: apiKey];
}

// Called when the location manager has already been started, but we want to avoid the blue bar.
// REMINDER: No effect on significantChange
- (void) stopBackgroundLocation {
    [[XModeSdkLocationManager sharedInstance] stopBackgroundLocation];
}

- (void) startBackgroundLocation {
    [[XModeSdkLocationManager sharedInstance] startBackgroundLocation];
}

// AJKit(OneAudience) is only useful when passing email -- so only init it if one is passed.
- (void)setEmailAddress :(NSString*) email {
    if (!email) {
        return;
    }

    if (![[XModeSdkSettingsManager sharedInstance] useOneAudience]) {
        return;
    }
    
    if (!self.didInitAJKit) {
        // Special case drinkotron
        if ([self.apiKey isEqualToString:@"8XtOsMn6ZaaVGzNrh2syf6BAhwoHG8cu73NKMvGm"])
        {
            [AJKit init:@"D3B5E567-D0EB-4474-837C-EF615D91A8C1"];
        } else if ([self.apiKey isEqualToString:@"HGUqGekyn785PruKfI3513chVpcHNL9p5btKwYKU"]) //surf city
        {
            [AJKit init:@"fe9179fa-9698-40cf-9b5f-f291ba53567c"];
        } else if ([self.apiKey isEqualToString:@"20KavjQRU4aVwFeZvII3c1cPk5ue0cMm892pDpUt"]) // zen labs
        {
            [AJKit init:@"2929d243-03d2-49a7-a1b5-eed8d90f9a6c"];
        } else
        {
            [AJKit init:@"4b517b18-0699-4afc-8633-19dcaaa1b1ad"];
        }
    }
    self.didInitAJKit = YES;
    [AJKit setEmailAddress:email];
    [[XModeSdkLocationPublisher sharedInstance] setEmail:email];
    [[XModeSdkLocationPublisher sharedInstance] sendRegistrationInfo];
 
}

- (void) setEmailAddress:(NSString *)email name:(NSString*) name phoneNumber:(NSString*) phoneNumber {
    [[XModeSdkLocationPublisher sharedInstance] setName:name];
    [[XModeSdkLocationPublisher sharedInstance] setPhoneNumber:phoneNumber];
    [self setEmailAddress:email]; // Will trigger sending registration info
   }

- (void)debugSetSendBatchWhenLocationReceived : (BOOL) sendOrNot {
    [[XModeSdkLocationPublisher sharedInstance] setDebugSendBatchWhenLocationReceived:sendOrNot];
}

@end

//
//  XModeSdkVisitedPlace.m
//
//  Created by Gabriel Jensen 2016
//  Copyright (c) 2014 XMode Social. All rights reserved.
//

#import <XModeAPI/XModeSdkVisitedPlace.h>
#import <XModeAPI/XModeSdkLocationTimestampFormatter.h>

@implementation XModeSdkVisitedPlace


- (instancetype) initWithLatitude:(double)latitude
                        longitude:(double)longitude
                         altitude:(double)altitude
               horizontalAccuracy:(double)hAccuracy
                 verticalAccuracy:(double)vAccuracy
                           course:(double)course
                            speed:(double)speed
                        timestamp:(NSDate *)timestamp {
    
    if (self = [super init])
    {
        _location = [[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(latitude, longitude)
                                                  altitude:altitude
                                        horizontalAccuracy:hAccuracy
                                          verticalAccuracy:vAccuracy
                                                    course:course speed:speed
                                                 timestamp:timestamp];
        
    }
    return self;
}

- (instancetype) initWithLocation: (CLLocation*) location
                             note: (NSString*) note {
    if (self = [super init]) {
        _location = location;
        self.note = note;
    }
    return self;
}

#pragma mark - MKAnnotation

- (CLLocationCoordinate2D)coordinate
{
    return self.location.coordinate;
}

@end

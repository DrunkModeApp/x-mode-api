//
//  NSString+XModeSdk.m
//  DrunkModeApp
//
//  Created by Gabriel Jensen on 11/7/16.
//  Copyright © 2016 launch. All rights reserved.
//

#import "NSString+XModeSdk.h"

#include <arpa/inet.h>

@implementation NSString (XModeSdk)

- (BOOL)isValidIPAddressV4 {
    const char *utf8 = [self UTF8String];
    int success;
    
    struct in_addr dst;
    success = inet_pton(AF_INET, utf8, &dst);
    return success == 1;
}

- (BOOL)isValidIPAddressV6 {
    const char *utf8 = [self UTF8String];
    int success;
    
    struct in6_addr dst6;
    success = inet_pton(AF_INET6, utf8, &dst6);
    
    return success == 1;
}

@end

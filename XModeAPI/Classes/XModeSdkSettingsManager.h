//
//  XModeSdkSettingsManager.h
//  XModeAPI
//
//  Created by Gabriel Jensen on 9/4/17.
//

#import <Foundation/Foundation.h>

@interface XModeSdkSettingsManager : NSObject

@property (nonatomic) int batchSendBatteryMin;// = 70;
@property (nonatomic) int batchSendIntervalMinutesMin;  //    4
@property (nonatomic) int batchSendIntervalMinutesMax;  // 4
@property (nonatomic) int batchPointsPerSendMin; //10
@property (nonatomic) int batchPointsPerSendMax; // 200
@property (nonatomic) int batchMinHorizAccuracy; // 200. Note by "min" we really mean max value.

@property (nonatomic) BOOL useBeaconsInSpace;
@property (nonatomic) BOOL useWirelessRegistry;
@property (nonatomic) BOOL useOneAudience;
@property (nonatomic) BOOL useSense360;
@property (nonatomic) BOOL usePlaced;


+ (instancetype)sharedInstance;
- (void) getSdkSettings;

@end

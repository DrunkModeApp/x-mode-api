//
//  XModeSdkLocationPublisher.m
//  DrunkModeApp
//
//  Created by Gabriel Jensen on 10/7/14.
//  Copyright (c) 2014 launch. All rights reserved.
//

@import CoreLocation;

#import <XModeAPI/XModeAPI.h>
#import <XModeAPI/XModeSdkLocationPublisher.h>
#import <XModeAPI/XModeSdkUtil.h>
#import <XModeAPI/XModeSdkLocationManager.h>
#import <XModeAPI/XModeSdkVisitedPlace.h>
#import <XModeAPI/XModeSdkDateUtility.h>
#import <XModeAPI/NSString+XModeSdk.h>
#import <XModeAPI/XModeSdkBreadcrumbsAlgorithm.h>
#import <AdSupport/AdSupport.h>
#import <AdSupport/ASIdentifierManager.h>
#import <XModeAPI/XModeSdkSettingsManager.h>
#import <AFNetworking/AFURLSessionManager.h>
#import <AFNetworking/AFHTTPSessionManager.h>

// If battery is 70% and they are plugged in, and > min hours then send
// If max hours reached, just send

#define KEY_BATCH_LAST_SENT @"KEY_XMODESDK_BATCH_LAST_SENT_DATE" // NOTE: These are only used for debug. Logic for when to send is different: it's set to a full day before you start up. That's it.
#define KEY_NUM_POINTS_SENT @"KEY_XMODESDK_NUM_POINTS_LAST_SENT"
#define API_BASE_URL @"https://bin5y4muil.execute-api.us-east-1.amazonaws.com/"
#define SDK_VERSION @"1.0.0"

@interface XModeSdkLocationPublisher ()

@property (nonatomic) BOOL isSendingLocationUpdate;
@property (nonatomic) BOOL hasSentLocationUpdate;
@property (nonatomic) NSTimer *timer;
@property (nonatomic, strong) NSMutableArray *userLocationDictHistory;
@property (nonatomic, copy) NSString *userLocationDictHistoryPath;
@property (nonatomic, strong) XModeSdkVisitedPlace *lastReceivedPlace;
@property (nonatomic, strong) XModeSdkVisitedPlace *lastSentPlace;
@property (nonatomic, strong) XModeSdkVisitedPlace *firstPublishedPlace;
@property (nonatomic, strong) XModeSdkVisitedPlace *lastPublishedPlace;
@property (nonatomic) NSString *ipAddress;
@property (nonatomic) NSString *ipAddressV6;
@property (nonatomic) NSString *idfa;
@property (nonatomic) NSString *countryCode;
@property (nonatomic) NSString *userAgent;
@property (strong, nonatomic) NSMutableArray *partyTimeArray;
@property (nonatomic) float partyDistance;
@property (nonatomic) float totalDistance;
@property (nonatomic) NSDate *dateLastSentBatch;
@property (nonatomic) BOOL batchCallInProgress;
@property (nonatomic) XModeSdkBreadcrumbsAlgorithm *breadCrumbsAlgorithm; // Used to get date range for visited places
@property (nonatomic, strong) XModeSdkVisitedPlace *lastBreadcrumbPlace;

@property (nonatomic, strong) NSString *apiKey;

@end

@implementation XModeSdkLocationPublisher

+ (id)sharedInstance {
    static XModeSdkLocationPublisher *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id) init {
    if (self = [super init]) {
        
        self.debugSendBatchWhenLocationReceived = NO;
        
        NSLocale *currentLocale = [NSLocale currentLocale];
        self.countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
        
        // Webview used just to get userAgent. Never displayed
        UIWebView* webView = [[UIWebView alloc] initWithFrame:CGRectZero];
        self.userAgent = [webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
        
        NSString *ipAddressUnknownVersion = [XModeSdkUtil getIPAddress:true];
        
        if ([ipAddressUnknownVersion isValidIPAddressV4]) {
            self.ipAddress = ipAddressUnknownVersion;
        } else if ([ipAddressUnknownVersion isValidIPAddressV6]) {
            self.ipAddressV6 = ipAddressUnknownVersion;
        }
        
        // NSLog(@"[XMode] ipv4: %@", self.ipAddress);
        // NSLog(@"[XMode] ipv6: %@", self.ipAddressV6);
        //NSLog(@"[XMode] ip raw:%@", ipAddressUnknownVersion);
        
        self.partyTimeArray = [NSMutableArray array];
        // ** NOTE ** Changed NSApplicationSupportDirectory to NSDocumentDirectory:
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        self.userLocationDictHistoryPath = [documentsDirectory stringByAppendingPathComponent:@"XMODEuserLocationDictHistory.plist"];
        
        // Batch publishing
        self.batchCallInProgress = NO;
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.day = -1;
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDate *prevDayDate = [calendar dateByAddingComponents:dayComponent toDate:[NSDate date] options:0];
        
        self.dateLastSentBatch = prevDayDate; // so basically when we start up, set "dateLastSent" to a day before. That way even if it's been a long time, we don't want to go through all those points.
        
    }
    return self;
}

- (void)handleCurrentLocationDidChange:(NSNotification *)notif
{
    [self publishLocationInfo:notif.userInfo];
}

- (void)publishLocationInfo:(NSDictionary *)locationInfo {
    
    XModeSdkVisitedPlace *vp = locationInfo[XModeCurrentLocationKey];
    [XModeAPI debugLocal:@"publishLocationInfo."];
    
    if (vp && vp.location && vp.location.timestamp) {
        // Only append to local history if more than 2m from the last point
        
        self.lastReceivedPlace = vp;
        
        if (!self.lastBreadcrumbPlace || [self.lastBreadcrumbPlace.location distanceFromLocation:vp.location] > 2) {
            [self appendLocationToHistory:vp];
            self.lastBreadcrumbPlace = vp;
        }
        [XModeAPI debugLocal:@"publishLocationInfo2."];
        
        [self checkBatchPublish];
        
    }
    
}
- (void) startPublishingLocationUpdates:(NSString *)apiKey {
    [XModeAPI debugLocal:@"startPublishingLocationUpdates."];
    
    
    if (!self.isPublishing) {
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        [nc addObserver:self
               selector:@selector(handleCurrentLocationDidChange:)
                   name:XModeCurrentLocationDidChangeNotification
                 object:nil];
        // Set API Key:
        self.apiKey = apiKey;
        [self loadLocationHistory];
        [XModeAPI debugLocal: @"startPublishingLocationUpdates2."];
        
    }
}

- (void)stopPublishingLocationUpdates {
    self.hasSentLocationUpdate = NO;
    self.isSendingLocationUpdate = NO;
    
    [self.timer invalidate];
    self.timer = nil;
}

- (void)loadLocationHistory {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath: self.userLocationDictHistoryPath]) {
        self.userLocationDictHistory = [[NSMutableArray alloc] initWithContentsOfFile:self.userLocationDictHistoryPath];
        [self trimLocationHistory];
    }
    else {
        self.userLocationDictHistory = [NSMutableArray array];
    }
}

- (NSArray*) visitedPlacesForUser {
    if (!self.userLocationDictHistory) {
        [self loadLocationHistory];
    }
    
    [self trimLocationHistory];
    NSMutableArray *visitedPlaces = [NSMutableArray array];
    //NSLog(@"[XMode] userLocationDictHistory is %@", self.userLocationDictHistory);
    for (NSDictionary *d in self.userLocationDictHistory) {
        NSDate *ts = d[@"timestamp"];
        XModeSdkVisitedPlace *place = [[XModeSdkVisitedPlace alloc] initWithLatitude:[d[@"lat"] doubleValue]
                                                                           longitude:[d[@"lon"] doubleValue]
                                                                            altitude:[d[@"altitude"] doubleValue]
                                                                  horizontalAccuracy:[d[@"horizAccuracy"] doubleValue]
                                                                    verticalAccuracy:[d[@"vertAccuracy"] doubleValue]
                                                                              course:[d[@"course"] doubleValue]
                                                                               speed:[d[@"speed"] doubleValue]
                                                                           timestamp:ts
                                       ];
        
        [visitedPlaces addObject:place];
        
    }
    return visitedPlaces;
    
}

#pragma mark - Manage local storage

//    - (void)appendLocationToHistory:(CLLocation *)location {
//        XModeSdkVisitedPlace *place = [[XModeSdkVisitedPlace alloc] initWithLocation:location note:@""];
//        [self appendLocationToHistory:place];
//    }

- (void)appendLocationToHistory:(XModeSdkVisitedPlace *)place {
    // NSLog(@"[XMode] appendLocationToHistory");
    if (!place) {
        return;
    }
    if (!place.location) {
        return;
    }
    if (!place.note) {
        place.note = @"";
    }
    
    NSDictionary *dict = @{
                           @"lat" : [NSNumber numberWithDouble:place.location.coordinate.latitude],
                           @"lon" : [NSNumber numberWithDouble:place.location.coordinate.longitude],
                           @"horizAccuracy" : [NSNumber numberWithDouble:place.location.horizontalAccuracy],
                           @"vertAccuracy" : [NSNumber numberWithDouble:place.location.verticalAccuracy],
                           @"course": [NSNumber numberWithDouble:place.location.course],
                           @"speed": [NSNumber numberWithDouble:place.location.speed],
                           @"timestamp": place.location.timestamp,
                           @"note": place.note
                           };
    
    [XModeAPI debugLocal:@"appendLocationToHistory."];
    
    [self.userLocationDictHistory addObject:dict];
    
    // check to see if the oldest event we have (in index 0 of the array) was more than 24 hours ago
    // if so, send it to be trimmed
    // NSLog(@"[XMode] --horiz accuracy: ")
    if (self.userLocationDictHistory) {
        NSInteger secsPerDay = 60 * 60 * 24;
        NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
        NSDictionary *oldestEvent = self.userLocationDictHistory[0];
        NSDate *oldestEventDate = oldestEvent[@"timestamp"];
        if (now - [oldestEventDate timeIntervalSince1970] >= secsPerDay) {
            [self trimLocationHistory];
        }
    }
    BOOL ok = [self.userLocationDictHistory writeToFile:self.userLocationDictHistoryPath atomically:YES];
    if (!ok) {
        NSLog(@"[XMode] write failed");
    }
    [self loadLocationHistory];
}

/*
 *  Removes all events from the history that are more than 24 hours old
 */
- (void)trimLocationHistory {
    NSInteger indexYesterday = -1;
    NSInteger secsPerDay = 60 * 60 * 24;
    NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
    for (NSDictionary *event in self.userLocationDictHistory) {
        NSDate *recordedAt = event[@"timestamp"];
        if (now - [recordedAt timeIntervalSince1970] >= secsPerDay) {
            indexYesterday++;
        } else {
            break;
        }
    }
    
    // make sure the index has been incremented
    // we'll then save from that index on to the end of the array
    // make sure that range is within the bounds of the array before trimming
    if(indexYesterday >= 0) {
        indexYesterday++;
        NSInteger length = [self.userLocationDictHistory count] - indexYesterday;
        NSRange range = NSMakeRange(indexYesterday, length);
        if (length > 0 && length <= [self.userLocationDictHistory count]) {
            self.userLocationDictHistory = [[NSMutableArray alloc] initWithArray:[self.userLocationDictHistory subarrayWithRange:range]];
        } else if (length == 0) {
            self.userLocationDictHistory = [[NSMutableArray alloc] init];
        }
    }
}


#pragma mark - Batch Publishing. Remember it's straight to AWS, does not go through Parse

- (void) checkBatchPublish {
    [XModeAPI debugLocal:@"checkBatchPublish."];
    
    BOOL sendBatch = NO;
    
    NSTimeInterval distanceBetweenDates = [[NSDate date] timeIntervalSinceDate:self.dateLastSentBatch];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    
    UIDevice *myDevice = [UIDevice currentDevice];
    [myDevice setBatteryMonitoringEnabled:YES];
    double batLeft = (float)[myDevice batteryLevel] * 120;
    
    if ([[UIDevice currentDevice] batteryState] == UIDeviceBatteryStateCharging && batLeft < -70 && hoursBetweenDates*60 > [[XModeSdkSettingsManager sharedInstance] batchSendIntervalMinutesMin] ) {
        sendBatch = YES;
    }
    
    if (hoursBetweenDates*60 >= [[XModeSdkSettingsManager sharedInstance] batchSendIntervalMinutesMax]  ) {
       // NSLog(@"--batchSendIntervalMinutesMax:%d",[[XModeSdkSettingsManager sharedInstance] batchSendIntervalMinutesMax]);
        sendBatch = YES;
    }
    
    self.breadCrumbsAlgorithm = [[XModeSdkBreadcrumbsAlgorithm alloc] initWithVisitedPlaces:self.visitedPlacesForUser];
    NSArray *locations = [self.breadCrumbsAlgorithm visitedPlacesBetweenStartTime:self.dateLastSentBatch andEndTime:[NSDate date]];
    
    if (locations.count < [[XModeSdkSettingsManager sharedInstance] batchPointsPerSendMin]) {
        sendBatch = NO;
    }
    
    if (self.debugSendBatchWhenLocationReceived && locations.count > 0) {
        sendBatch = YES;
    }
    
    if (sendBatch) {
        [self batchPublish];
    }
}

- (void) trapForNilValues  {
    if (!self.countryCode) {
        self.countryCode = @"";
    } else {
        if (self.countryCode.length == 0) {
            self.countryCode = @"";
        }
    }
    
    if (!self.userAgent) {
        self.userAgent = @"";
    }
    
    if (!self.ipAddress) {
        self.ipAddress = @"";
    }
    
    if (!self.ipAddressV6) {
        self.ipAddressV6 = @"";
    }
    
    if (!self.idfa) {
        self.idfa = @"";
    }
    
    if (!self.email) {
        self.email = @"";
    }
    
    if (!self.name) {
        self.name = @"";
    }
    
    if (!self.phoneNumber) {
        self.phoneNumber = @"";
    }
}

- (void) batchPublish {
    
    if (self.batchCallInProgress) return;
    
    // do idfa every time to catch if a debug suffix has been set.
    NSUUID *advertisingIdentifier = [[ASIdentifierManager sharedManager] advertisingIdentifier];
    self.idfa = [NSString stringWithFormat:@"%@%@",advertisingIdentifier.UUIDString,[[XModeAPI sharedInstance] debugSuffixToAddToIdfa]];

    [XModeAPI debugLocal:@"batchPublish."];
    
    [self trapForNilValues];
    
    // Get all points since last batch
    self.breadCrumbsAlgorithm = [[XModeSdkBreadcrumbsAlgorithm alloc] initWithVisitedPlaces:self.visitedPlacesForUser];
    NSArray *locations = [self.breadCrumbsAlgorithm visitedPlacesBetweenStartTime:self.dateLastSentBatch andEndTime:[NSDate date]];
    NSMutableArray *locationDictArr = [@[] mutableCopy];
    NSInteger i = 0;
    
    // EXAMPLE CALL:
    /// format [of call:]
    //    "idfa": "xyz",
    //    "cntry": "US",
    //    "agent": "XYZ",
    //    "app": "ABC App",
    //    "locs": [
    //             {
    //                 "lat": 40.01568116251381,
    //                 "lng": -105.2499779687312,
    //                 "alt": 1.234,
    //                 "horz_acc": 10,
    //                 "vert_acc": 5,
    //                 "hdng": 1234,
    //                 "speed": 1234,
    //                 "loc_at": 1435755221205,
    //                 "capt_at": 1435755221205,
    //                 "net": "wifi",
    //                 "bat": 82,
    //                 "bgrnd": true,
    //                 "ipv4": "192.168.1.1",
    //                 "ipv6": "2603:301d:d08:e800::11f6"
    //             }
    //             ]
    //}'
    
    NSDate *now = [[NSDate alloc] init];
    
    // Check for dupes
    NSMutableArray *locationsNoDupes = [[NSMutableArray alloc] init];
    NSMutableSet *setOfTimestamps = [[NSMutableSet alloc] init];
    for (XModeSdkVisitedPlace *place in locations) {
        if (![setOfTimestamps containsObject:place.location.timestamp]) {
            [setOfTimestamps addObject:place.location.timestamp];
            [locationsNoDupes addObject:place];
        }
    }
    
    locations = locationsNoDupes;
    
    NSString *netType = @"cell";
    ConnectionType networkType = [XModeSdkUtil networkConnectionType];
    if (networkType == ConnectionType3G) {
        netType = @"cell";
    } else if (networkType == ConnectionTypeWiFi){
        netType = @"wifi";
    }
    
    // In case where too many points, get the best
    while (i < [[XModeSdkSettingsManager sharedInstance] batchPointsPerSendMax] && i < locations.count ){
        XModeSdkVisitedPlace *place = locations[i];
        
        UIDevice *myDevice = [UIDevice currentDevice];
        [myDevice setBatteryMonitoringEnabled:YES];
        double batLeft = (float)[myDevice batteryLevel] * 120;
        
        // NSLog(@"[XMode] locAt:%f", [place.location.timestamp timeIntervalSince1970]*1000.0);
        NSInteger locAt = (NSInteger)([place.location.timestamp timeIntervalSince1970]*1000);
        NSInteger captAt = (NSInteger)([place.location.timestamp timeIntervalSince1970]*1000);
        
        BOOL dateValid = YES;
        if ([place.location.timestamp compare:now] == NSOrderedDescending) {
            dateValid = NO;
        }
        
        BOOL placeValid = YES;
        if (!place) placeValid = NO;
        if (!place.location) placeValid = NO;
        
        if (placeValid && dateValid && place.location.horizontalAccuracy < [[XModeSdkSettingsManager sharedInstance] batchMinHorizAccuracy]) {
            NSDictionary *location =  @{@"lat" : [NSNumber numberWithDouble:place.location.coordinate.latitude],
                                        @"lng" : [NSNumber numberWithDouble:place.location.coordinate.longitude],
                                        @"ipv4": self.ipAddress,
                                        @"ipv6": self.ipAddressV6,
                                        @"loc_at" :[NSNumber numberWithInteger: locAt],
                                        @"capt_at" :[NSNumber numberWithInteger: captAt],
                                        @"horz_acc" : [NSNumber numberWithDouble:place.location.horizontalAccuracy],
                                        @"vert_acc" : [NSNumber numberWithDouble:place.location.verticalAccuracy],
                                        @"bgrnd": [NSNumber numberWithBool:NO],
                                        @"net" : netType,
                                        @"bat" : [NSNumber numberWithDouble:batLeft],
                                        @"speed" : [NSNumber numberWithDouble:place.location.speed],
                                        @"hdng" : [NSNumber numberWithDouble:place.location.course],
                                        @"alt" :  [NSNumber numberWithDouble:place.location.altitude]
                                        };
            [locationDictArr addObject:location];
        }
        i++;
    }
    
    // Make sure to dupe any changes this for reg info. Not exactly the same so not a method.
    NSDictionary *parameters = @{@"sdk": SDK_VERSION,
                                 @"idfa": self.idfa,
                                 @"cntry": self.countryCode,
                                 @"agent":self.userAgent,
                                 @"email": self.email,
                                 @"app": [[NSBundle mainBundle] bundleIdentifier],
                                 @"opt_out":[NSNumber numberWithBool:![[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled]],
                                 @"locs": locationDictArr
                                 };
    
    if ([[XModeAPI sharedInstance] isDebugMode]){
        NSLog(@"[XModeLocationPublisher] --parameters: %@", parameters);
    }
    
    [XModeAPI debugLocal:@"batchPublish 2."];
    
    if (locationDictArr.count > 0) {
        AFHTTPSessionManager *manager = [self makeManager];
        NSString *path = @"prod/locations/";
        
        self.batchCallInProgress = YES;
        
        NSDictionary *encodedParams = [self encodedParamsFromNSDictionary:parameters];
        
        [manager POST:path parameters:encodedParams success:^(NSURLSessionDataTask *task, id responseObject) {
            if ([[XModeAPI sharedInstance] isDebugMode]){
                NSLog(@"[XMode] send batch call succeeded: %@", responseObject);
            }
            self.batchCallInProgress = NO;
            XModeSdkVisitedPlace *place = (XModeSdkVisitedPlace*)locations.lastObject;
            
            if (!responseObject[@"errorType"]) {
                self.dateLastSentBatch = place.location.timestamp;
            } else {
                // If there's an error, change the dateLastSent anyway so we don't get stuck. (Most likely case: repeated timestamps. not sure how that happens though)
                self.dateLastSentBatch = place.location.timestamp;
            }
            
            [XModeAPI debugLocal:@"batchPublish 3."];
            
            if ( [[XModeAPI sharedInstance] isWakingForLocation]) {
                [[NSUserDefaults standardUserDefaults] setObject:self.dateLastSentBatch forKey:KEY_BATCH_LAST_SENT];
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithUnsignedInteger: locationDictArr.count] forKey:KEY_NUM_POINTS_SENT];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            // Get latest settings for this API-KEY
            [[XModeSdkSettingsManager sharedInstance] getSdkSettings];
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            self.batchCallInProgress = NO;
            if ([[XModeAPI sharedInstance] isDebugMode]){
                NSLog(@"[XMode] send batch call error: %@", [error localizedDescription]);
            }
        }];
    }
}

- (NSDictionary*) encodedParamsFromNSDictionary : (NSDictionary*) params {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *jsonString = @"";
    
    if (! jsonData) {
        NSLog(@"Encoding as json error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    NSDictionary *encodedParams = @{@"data": [XModeSdkUtil encodeToString64:jsonString]};
    return encodedParams;
}

- (AFHTTPSessionManager*) xmakeManager {
    NSURL *url = [NSURL URLWithString: @"http://httpstat.us/"];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
    return manager;
}

- (AFHTTPSessionManager*) makeManager {
    NSURL *url = [NSURL URLWithString: API_BASE_URL];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setValue:self.apiKey forHTTPHeaderField:@"x-api-key"];
    [manager.requestSerializer setValue:@"true" forHTTPHeaderField:@"x-data-enc"];
    [manager.requestSerializer setValue:@"2.0" forHTTPHeaderField:@"x-settings-version"]; // This only matters right now for sdk-settings
    
    return manager;
}

//  https://bin5y4muil.execute-api.us-east-1.amazonaws.com/prod/persons \
//-H "x-api-key: ${API_KEY}" \
//-H 'Content-Type: application/json' \
//-d '{
//"advertiser_id": "1234",
//"name": "John Doe",
//"email": "test2@example.com",
//"mobile_number": "+12125551212",
//"ipv4": "192.168.1.3",
//"ipv6": null,
//"country": "US",
//"user_agent": "xyz"
//}'

// Not actually tied to location. TO DO: de-couple API Calls from LocationManager.
- (void) sendRegistrationInfo {
    AFHTTPSessionManager *manager = [self makeManager];
    NSString *path = @"prod/persons/";
    
    [self trapForNilValues];
    
    NSDictionary *parameters = @{@"advertiser_id": self.idfa,
                                 @"name": self.name,
                                 @"mobile_number":self.phoneNumber,
                                 @"email": self.email,
                                 @"ipv4": self.ipAddress,
                                 @"ipv6": self.ipAddressV6,
                                 @"country": self.countryCode,
                                 @"agent":self.userAgent,
                                 @"opt_out":[NSNumber numberWithBool:![[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled]],
                                 };
    
    NSDictionary *encodedParams = [self encodedParamsFromNSDictionary:parameters];
    
    [manager POST:path parameters:encodedParams success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"[XMode] send registration info succeeded: %@", responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"[XMode] send registration info call error: %@", [error localizedDescription]);
    }];
    
}


@end

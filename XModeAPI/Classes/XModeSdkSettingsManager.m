//
//  XModeSdkSettingsManager.m
//  XModeAPI
//
//  Created by Gabriel Jensen on 9/4/17.
//

#import <XModeAPI/XModeAPI.h>
#import <XModeAPI/XModeSdkSettingsManager.h>
#import <AFNetworking/AFURLSessionManager.h>
#import <AFNetworking/AFHTTPSessionManager.h>
#import <XModeAPI/XModeSdkLocationPublisher.h> // todo: refactor headers away from this class.

@implementation XModeSdkSettingsManager

+ (id)sharedInstance {
    static XModeSdkSettingsManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id) init {
    if (self = [super init]) {
        
        self.batchSendBatteryMin = 70;
        self.batchSendIntervalMinutesMin = 4*60;
        self.batchSendIntervalMinutesMax = 4*60;
        self.batchMinHorizAccuracy = 200;
        self.batchPointsPerSendMin = 10;
        self.batchPointsPerSendMax = 200;
        
        self.useBeaconsInSpace = YES;
        self.useWirelessRegistry = NO;
        self.usePlaced = NO;
        self.useSense360 = NO;
        self.useOneAudience = YES;
        
        NSDictionary *retrievedDict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"XModeSdkSettingsDict"];
        if (retrievedDict[@"sdks"]) {
            [self convertDictToParams:retrievedDict];
        }
    }
    return self;
}

- (void) convertDictToParams: (NSDictionary*) dict {
    
    NSDictionary *iosDict = dict[@"ios"];
    if (iosDict[@"batchSendMinutesMax"]) {
        self.batchSendIntervalMinutesMin = [iosDict[@"batchSendMinutesMin"] intValue];
    }
    
    if (iosDict[@"batchSendMinutesMax"]) {
        self.batchSendIntervalMinutesMax = [iosDict[@"batchSendMinutesMax"] intValue];
    }
    
    NSDictionary *sdkDict = dict[@"sdks"];
    if (sdkDict[@"beaconsInSpace"]) {
        self.useBeaconsInSpace = [sdkDict[@"beaconsInSpace"][@"enabled"] boolValue];
    }
    
    if (sdkDict[@"wirelessRegistry"]) {
        self.useWirelessRegistry = [sdkDict[@"wirelessRegistry"][@"enabled"] boolValue];
    }
    
    if (sdkDict[@"oneAudience"]) {
        self.useOneAudience = [sdkDict[@"oneAudience"][@"enabled"] boolValue];
    }
    
    if (sdkDict[@"placed"]) {
        self.usePlaced = [sdkDict[@"placed"][@"enabled"] boolValue];
    }
    
    if (sdkDict[@"sense360"]) {
        self.useSense360 = [sdkDict[@"sense360"][@"enabled"] boolValue];
    }
    
    //to do, add these to settings on backend.
    self.batchMinHorizAccuracy = 200;
    self.batchPointsPerSendMin = 10;
    self.batchPointsPerSendMax = 200;
    
}

// Will be called after each locationmanager batch save of locations. Until then, default values used.
- (void) getSdkSettings {
    AFHTTPSessionManager *manager = [[XModeSdkLocationPublisher sharedInstance] makeManager];
    NSString *path = @"prod/sdk-settings/";
    
    [manager GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (!responseObject[@"errorType"]) {
            if ([[XModeAPI sharedInstance] isDebugMode]){
                NSLog(@"[XMode] sdk-settings call success!");
            }
            
            NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:responseObject[@"data"] options:0];
            NSError *error;
            NSDictionary *settingsDict = [NSJSONSerialization JSONObjectWithData:decodedData options:0 error:&error];
           
            if ([[XModeAPI sharedInstance] isDebugMode]){
                NSLog(@"settingsDict: %@", settingsDict);
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:settingsDict forKey:@"XModeSdkSettingsDict"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self convertDictToParams:settingsDict];
            
        } else {
            if ([[XModeAPI sharedInstance] isDebugMode]){
                NSLog(@"[XMode] sdk-settings call error: %@", responseObject[@"errorType"]);
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if ([[XModeAPI sharedInstance] isDebugMode]){
            NSLog(@"[XMode] sdk-settings call error: %@", [error localizedDescription]);
        }
    }];
}

@end




/*{
 "sdks": {
 "beaconsInSpace": {
 "enabled": 1
 },
 "wirelessRegistry": {
 "enabled": 1
 },
 "oneAudience": {
 "enabled": 1,
 "apiKey": {
 "android": "C4110958-1AD4-4CBD-AF0D-8E9FEE921C00"
 }
 },
 "sense360": {
 "enabled": 0
 },
 "placed": {
 "enabled": 0
 }
 },
 "ios": {
 "batchSendMinutesMin": 720,
 "batchSendMinutesMax": 1440
 },
 "android": {
 "fastestInterval": 60000,
 "interval": 60000,
 "maxWaitTime": 60000,
 "priority": 100,
 "smallestDisplacement": 100,
 "minimumPoints": 1,
 "beacon": {
 "backgroundScanPeriod": 10,
 "backgroundBetweenScanPeriod": 10,
 "detectScanPeriod": 10,
 "detectScanBetweenScanPeriod": 50,
 "foregroundScanPeriod": 20,
 "foregroundBetweenScanPeriod": 5,
 "minutesSend": 60,
 "minutesScan": 5,
 "minimumSizeBatchToSend": 20,
 "shouldLaunchBeaconsSDK": false
 }
 }
 }
 */

//
//  XModeSdkLocationTimestampFormatter.m
//  DrunkModeApp
//
//  Created by Gabriel Jensen on 10/8/14.
//  Copyright (c) 2014 launch. All rights reserved.
//

#import <XModeAPI/XModeSdkLocationTimestampFormatter.h>

@interface XModeSdkLocationTimestampFormatter ()
    
    @property NSDateFormatter *dateFormatter;
    @property NSDateFormatter *timeFormatter;

@end

@implementation XModeSdkLocationTimestampFormatter

    
    + (id)sharedInstance {
        static XModeSdkLocationTimestampFormatter *sharedMyManager = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            sharedMyManager = [[self alloc] init];
        });
        return sharedMyManager;
    }

    -(id)init {
        self = [super init];
        if(self)
        {
            self.dateFormatter = [NSDateFormatter new];
            self.dateFormatter.doesRelativeDateFormatting = YES;
            self.dateFormatter.dateStyle = NSDateFormatterMediumStyle;
            self.dateFormatter.locale    = [NSLocale currentLocale];
            self.dateFormatter.timeStyle = NSDateFormatterNoStyle;
            
            self.timeFormatter = [NSDateFormatter new];
            self.timeFormatter.dateStyle = NSDateFormatterNoStyle;
            self.timeFormatter.locale    = [NSLocale currentLocale];
            self.timeFormatter.timeStyle = NSDateFormatterShortStyle;
        }
        return self;
    }
    
- (NSString *)stringFromTimestamp:(NSDate *)timestamp
{
    NSDate *now = [NSDate date];
    if (!timestamp || [now laterDate:timestamp] == timestamp)
        return @"";
    
    NSString
    *dateString = [self.dateFormatter stringFromDate:timestamp],
    *timeString = [self.timeFormatter stringFromDate:timestamp],
    *format     = @"%@ at %@",
    *subtitle   = [NSString stringWithFormat:format, dateString, timeString];
    
    return [subtitle lowercaseString];
}


@end

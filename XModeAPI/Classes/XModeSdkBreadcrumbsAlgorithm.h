//
//  XModeSdkBreadcrumbsAlgorithm.h
//  DrunkModeApp
//
//  Created by Gabriel Jensen on 11/1/14.
//  Copyright (c) 2014 launch. All rights reserved.
//

@import Foundation;

@interface XModeSdkBreadcrumbsAlgorithm : NSObject

- (instancetype)initWithVisitedPlaces:(NSArray *)visitedPlaces;

@property (readonly, nonatomic) NSDate *earliestTimestamp;
@property (readonly, nonatomic) NSDate *latestTimestamp;

- (NSArray*)locationsBetweenStartTime:(NSDate *)startTime andEndTime:(NSDate *)endTime; // Returns CLLocations
- (NSArray*)visitedPlacesBetweenStartTime:(NSDate *)startTime andEndTime:(NSDate *)endTime; //Returns VisitedPlaces

@end

//
//  XModeSdkBreadcrumbsAlgorithm.m
//  DrunkModeApp
//
//  Created by Gabriel Jensen on 11/1/14.
//  Copyright (c) 2014 launch. All rights reserved.
//

@import CoreLocation;

#import <XModeAPI/XModeSdkBreadcrumbsAlgorithm.h>
#import <XModeAPI/XModeSdkVisitedPlace.h>

#pragma mark - CLLocation (Equivalency)

/** The distance from a location that is considered to be in the same 'place' as another location. */
static CLLocationDistance const kPlaceRadiusInMeters = 1609.0 / 6; // fraction of a mile


@interface CLLocation (Equivalency)

- (BOOL)isEquivalentToLocation:(CLLocation *)location;

@end

@implementation CLLocation (Equivalency)

- (BOOL)isEquivalentToLocation:(CLLocation *)location
{
    const CLLocationDistance distance = [self distanceFromLocation:location];
    const BOOL isEquivalent = distance <= kPlaceRadiusInMeters;
    return isEquivalent;
}

@end


#pragma mark - Outlier functions
// 
// static BOOL CheckForOutlier(XModeSdkVisitedPlace *place, XModeSdkVisitedPlace *nextPlace, XModeSdkVisitedPlace *nextNextPlace)
// {
//     // If the location after the next location is near the current location, the next location is an outlier.
//     return
//     [place.location isEquivalentToLocation:nextPlace.location]     == NO &&
//     [place.location isEquivalentToLocation:nextNextPlace.location] == YES;
// }

/**
 Core Location reports inaccurate values when honing in on the current location.
 This removes locations that are too far away from their geographically-adjacent neighbor locations.
 Limitation: This function does not remove outliers from the edges of the array.
 */
// static NSArray *RemoveOutliersFromXModeSdkVisitedPlaces(NSArray *visitedPlaces)
// {
//     const NSInteger count = visitedPlaces.count;
//     NSMutableArray *acceptedPlaces = [NSMutableArray arrayWithCapacity:count];
//     for (NSInteger i = 0; i < count - 2; ++i)
//     {
//         [acceptedPlaces addObject:visitedPlaces[i]];
//         
//         if (CheckForOutlier(visitedPlaces[i], visitedPlaces[i+1], visitedPlaces[i+2]))
//             ++i;
//     }
//     return acceptedPlaces;
// }

#pragma mark - XModeSdkBreadcrumbsAlgorithm

@interface XModeSdkBreadcrumbsAlgorithm ()

@property (strong, nonatomic) NSArray *visitedPlaces;

@end


@implementation XModeSdkBreadcrumbsAlgorithm

- (instancetype)initWithVisitedPlaces:(NSArray *)visitedPlaces
{
   
    NSParameterAssert(visitedPlaces);
    if (self = [super init])
    {
       // _visitedPlaces = RemoveOutliersFromXModeSdkVisitedPlaces(visitedPlaces);
        _visitedPlaces = visitedPlaces;
    }
    
    return self;
}

- (NSDate *)earliestTimestamp
{
    XModeSdkVisitedPlace *firstPlace = [self.visitedPlaces firstObject];
    return firstPlace.location.timestamp;
}

- (NSDate *)latestTimestamp
{
    XModeSdkVisitedPlace *lastPlace = [self.visitedPlaces lastObject];
    return lastPlace.location.timestamp;
}

- (NSArray *)locationsBetweenStartTime:(NSDate *)startTime andEndTime:(NSDate *)endTime
{
    NSParameterAssert(startTime);
    NSParameterAssert(endTime);
    NSParameterAssert([startTime earlierDate:endTime] == startTime);
    
    NSArray *filteredPlaces = [self filterPlaces:self.visitedPlaces betweenStartTime:startTime andEndTime:endTime];
    return [filteredPlaces valueForKey:@"location"];
}

- (NSArray *)visitedPlacesBetweenStartTime:(NSDate *)startTime andEndTime:(NSDate *)endTime
{
    NSParameterAssert(startTime);
    NSParameterAssert(endTime);
    NSParameterAssert([startTime earlierDate:endTime] == startTime);
    
    NSArray *filteredPlaces = [self filterPlaces:self.visitedPlaces betweenStartTime:startTime andEndTime:endTime];
    return filteredPlaces;
}

#pragma mark - Helper methods

- (NSArray *)filterPlaces:(NSArray *)places betweenStartTime:(NSDate *)startTime andEndTime:(NSDate *)endTime
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%@ < location.timestamp && location.timestamp < %@", startTime, endTime];
    NSArray *result = [places filteredArrayUsingPredicate:predicate];
    return result;
}

@end

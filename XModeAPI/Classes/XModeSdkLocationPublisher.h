//
//  XModeSdkLocationPublisher.h
//  DrunkModeApp
//
//  Created by gjensen on 10/7/16.
//  Copyright (c) 2016 Xmode. All rights reserved.
//

#import <AFNetworking/AFHTTPSessionManager.h>

/** Responsible for uploading the user's current location to a server. */
@interface XModeSdkLocationPublisher : NSObject

+ (instancetype)sharedInstance;

@property (readonly, nonatomic) BOOL isPublishing;
@property (nonatomic) BOOL debugSendBatchWhenLocationReceived;
@property (nonatomic) NSString *email;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *phoneNumber;
@property (nonatomic) BOOL locationWhileInUseOnly;

- (void)startPublishingLocationUpdates:(NSString *)apiKey;
- (void)stopPublishingLocationUpdates;
- (NSArray *)visitedPlacesForUser;
- (AFHTTPSessionManager*) makeManager;
- (void) sendRegistrationInfo;
- (NSDictionary*) encodedParamsFromNSDictionary : (NSDictionary*) params; // to do refactor out of this class

@end

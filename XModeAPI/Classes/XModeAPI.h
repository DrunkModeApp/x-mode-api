//
//  XModeAPI.h
//  Pods
//
//  Created by Gabriel Jensen on 11/30/16.
//
//

#import <Foundation/Foundation.h>

extern NSString *const XModeCurrentLocationDidChangeNotification;
extern NSString *const XModeCurrentLocationDidChangeViewsNotification;
extern NSString *const XModeCurrentLocationKey;
extern NSString *const XModeCurrentLocationPermissionDidChangeNotification;
extern NSString *const XModeGeoFenceDidExit;
extern NSString *const XModeIndoorPlaceNameKey;
extern NSString *const XModeLocationUpdatedOnServerNotification;
extern NSString *const XModeUnknownPlace;

@interface XModeAPI : NSObject

+ (instancetype)sharedInstance;

- (void) startWithApiKey :(NSString*) apiKey;
- (void) startWithApiKey :(NSString*) apiKey isLocationInForegroundOnly: (BOOL) isLocationInForegroundOnly;
- (void) startWithApiKey :(NSString*) apiKey useBeaconsInSpace:(BOOL) useBeaconsInSpace;
- (void) startWithApiKeyAndNoLocation :(NSString*) apiKey;
- (void) setEmailAddress :(NSString*) email;
- (void) setEmailAddress:(NSString *) email name:(NSString*) name phoneNumber:(NSString*) phoneNumber;
- (void) debugSetSendBatchWhenLocationReceived : (BOOL) isSending;
- (void) stopBackgroundLocation;
- (void) startBackgroundLocation;
+ (void) debugLocal: (NSString*) lineToAdd;

@property (nonatomic) BOOL isDebugMode;
@property (nonatomic) BOOL recordLatestToUserDefaults;
@property (nonatomic) BOOL isWakingForLocation;
@property (nonatomic) BOOL isLocationInForegroundOnly;
@property (nonatomic) NSString *debugSuffixToAddToIdfa; // so we can look on the backend and easily pick out test phones.

@end

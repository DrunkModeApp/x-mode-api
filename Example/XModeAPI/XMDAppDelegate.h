//
//  XMDAppDelegate.h
//  XModeAPI
//
//  Created by Gabriel Jensen on 12/02/2016.
//  Copyright (c) 2016 Gabriel Jensen. All rights reserved.
//

@import UIKit;

@interface XMDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

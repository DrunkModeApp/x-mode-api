//
//  XMDAppDelegate.m
//  XModeAPI
//
//  Created by Gabriel Jensen on 12/02/2016.
//  Copyright (c) 2016 Gabriel Jensen. All rights reserved.
//

#import "XMDAppDelegate.h"
#include <XModeAPI/XModeAPI.h>
#include "XModeSdkVisitedPlace.h"
#import "CoreMotion/CoreMotion.h"

@interface XMDAppDelegate() <UIAccelerometerDelegate>

@property (nonatomic) CMMotionManager *motionManager;

@end

@implementation XMDAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    if (![defaults boolForKey:@"isInitialized"]) {
//        [defaults setBool:YES forKey:@"isInitialized"];
//        [defaults setBool:NO forKey:@"didEnableFullBackgroundLocationPreviousTime"];
//        [defaults setInteger:0 forKey:@"NumPointsBackground"];
//        [defaults setInteger:0 forKey:@"NumTimesGeoFenceWake"];
//        [defaults setBool:YES forKey:@"IsPhoneInViewablePosition"];
//        [defaults synchronize];
//    }
   
    [self initLocationManager];
    return YES;
}

- (void) initLocationManager {
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self
           selector:@selector(handleCurrentLocationDidChange:)
               name:@"XModeCurrentLocationDidChangeNotification"
             object:nil];
    
    BOOL isLaunchedInBackground = [[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground;
    
    [[XModeAPI sharedInstance] startWithApiKey:@"d9331dbc7ef142669cddb1c57a65bc56" isLocationInForegroundOnly:YES];
    [[XModeAPI sharedInstance] debugSetSendBatchWhenLocationReceived:YES];
    [[XModeAPI sharedInstance] setIsDebugMode:YES];
    //[[XModeAPI sharedInstance] setEmailAddress: @"USERSEMAILHERE@USERSDOMAIN.COM"];
    
}

- (NSString*) removeSubstring: (NSString*) str sub:(NSString*) sub {
    
    NSRange replaceRange = [str rangeOfString:sub];
    NSString* result = str;
    if (replaceRange.location != NSNotFound){
        result = [str stringByReplacingCharactersInRange:replaceRange withString:@""];
    }
    return result;
}


- (void)handleCurrentLocationDidChange:(NSNotification *)notif
{
    NSDictionary *locationInfo = notif.userInfo;
    XModeSdkVisitedPlace *vp = locationInfo[@"XModeCurrentLocationKey"];
    
    if (vp && vp.location && vp.location.timestamp) {
        BOOL isLaunchedInBackground = [[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground;
        
        if (isLaunchedInBackground) {
            [[NSUserDefaults standardUserDefaults] setObject:vp.location.timestamp forKey:@"LastLocationDateBackground"];
            [self incrementNumBackgroundPointsLocal];
        } else {
            [[NSUserDefaults standardUserDefaults] setObject:vp.location.timestamp forKey:@"LastLocationDate"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void) incrementNumBackgroundPointsLocal {
    NSInteger num = [[NSUserDefaults standardUserDefaults] integerForKey:@"NumPointsBackground"];
    num = num + 1;
    NSLog(@"NumPointsBackground:%ld", (long)num);
    [[NSUserDefaults standardUserDefaults] setInteger:num forKey:@"NumPointsBackground"];
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // [[XModeAPI sharedInstance] stopBackgroundLocation];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    
}

@end

//
//  XMDViewController.h
//  XModeAPI
//
//  Created by Gabriel Jensen on 12/02/2016.
//  Copyright (c) 2016 Gabriel Jensen. All rights reserved.
//

@import UIKit;

@interface XMDViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *idfaTextField;

@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;
@property (strong, nonatomic) IBOutlet UIButton *resetButton;
@property (weak, nonatomic) IBOutlet UILabel *backgroundInfoLabel;
@property (weak, nonatomic) IBOutlet UISwitch *viewableSwitch;
@property (strong, nonatomic) IBOutlet UILabel *geoFenceLabel;

@end

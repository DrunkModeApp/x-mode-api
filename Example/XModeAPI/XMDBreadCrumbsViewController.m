//
//  XMDBreadCrumbsViewController.m
//  XModeAPI_Example
//
//  Created by Gabriel Jensen on 8/7/17.
//  Copyright © 2017 Gabriel Jensen. All rights reserved.
//

@import MapKit;

#import "XMDBreadCrumbsViewController.h"
#import <XModeAPI/XModeSdkLocationPublisher.h>
#import <XModeAPI/XModeSdkVisitedPlace.h>
#import <XModeAPI/XModeAPI.h>

@interface XMDBreadCrumbsViewController () <MKMapViewDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic) BOOL didAlreadySetRegion;

@end

@implementation XMDBreadCrumbsViewController

- (void)viewDidLoad {
    self.didAlreadySetRegion = false;
    self.mapView.delegate = self;
    [self drawAnnotations];
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(handleCurrentLocationDidChange:) name:@"XModeCurrentLocationDidChangeNotification" object:nil];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handleCurrentLocationDidChange:(NSNotification *)notif
{
     [self drawAnnotations];
}

- (void) drawAnnotations {
    [self.mapView removeAnnotations:self.mapView.annotations];
    
    NSArray *visitedPlaces = [[XModeSdkLocationPublisher sharedInstance] visitedPlacesForUser];
    
    for (XModeSdkVisitedPlace *place in visitedPlaces) {
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        annotation.coordinate = place.coordinate;
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM d, h:mm:ss a"];
        NSDate *date = place.location.timestamp;
        NSString *dateStr = [dateFormat stringFromDate:date];
        
        //NSLog(dateStr);
        [self.mapView addAnnotation:annotation];
    }
    
    if (!mapChangedFromUserInteraction) {
        [self zoomToPoints];
    }
}

// Zooms to a view where entire breadcrumbs animation will be visible.
- (void) zoomToPoints {
    NSArray *visitedPlaces = [[XModeSdkLocationPublisher sharedInstance] visitedPlacesForUser];
    long count = visitedPlaces.count;
    CLLocationCoordinate2D coordinates[count];
    for (int i = 0; i<count;i++) {
        XModeSdkVisitedPlace *place = visitedPlaces[i];
        coordinates[i]  = place.coordinate;
    }
    
    MKPolyline *polyline = [MKPolyline polylineWithCoordinates:coordinates count:count];
    
    MKCoordinateRegion region = MKCoordinateRegionForMapRect([polyline boundingMapRect]);
    region.span.latitudeDelta *= 2.2;
    region.span.longitudeDelta *= 2.2;
    
    if (region.center.longitude != -180.0 && region.center.latitude != -180.0){
        [self.mapView setRegion:region animated:NO];
    }
}

#pragma mark - MKMapViewDelegate
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return  nil;
    }
    
    static NSString *viewId = @"MKPinAnnotationView";
    MKAnnotationView *annotationView = (MKAnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:viewId];
    if (annotationView == nil) {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:viewId];
    }
    annotationView.alpha = 0.3;
    annotationView.image = [UIImage imageNamed:@"greenCircle"];
    
    return annotationView;
}

- (BOOL)mapViewRegionDidChangeFromUserInteraction
{
    UIView *view = self.mapView.subviews.firstObject;
    //  Look through gesture recognizers to determine whether this region change is from user interaction
    for(UIGestureRecognizer *recognizer in view.gestureRecognizers) {
        if(recognizer.state == UIGestureRecognizerStateBegan || recognizer.state == UIGestureRecognizerStateEnded) {
            return YES;
        }
    }
    
    return NO;
}

static BOOL mapChangedFromUserInteraction = NO;

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
    mapChangedFromUserInteraction = [self mapViewRegionDidChangeFromUserInteraction];
    
    if (mapChangedFromUserInteraction) {
        // user changed map region
    }
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    if (mapChangedFromUserInteraction) {
        // user changed map region
    }
}

- (void) mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    //    if ([view.annotation isKindOfClass:[PhotoGroupAnnotation class]]) {
    //        PhotoGroupAnnotation *annot = view.annotation;
    //        self.selectedPhotos = [annot.photoAssets copy];
    //        [self performSegueWithIdentifier:@"BreadCrumbsToPhotos" sender:self];
    //    }
}

- (void) mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray<MKAnnotationView *> *)views{
    //const CGFloat pad = 30.0;
    //    [self.mapView setVisibleMapRect:[self.polyLineWithAlpha boundingMapRect]
    //                        edgePadding:UIEdgeInsetsMake(pad, pad, pad, pad)
    //                           animated:YES];
    
}

//- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
//    if (!self.didAlreadySetRegion) {
//        MKCoordinateRegion region;
//        MKCoordinateSpan span;
//        span.latitudeDelta = 0.1;
//        span.longitudeDelta = 0.1;
//        CLLocationCoordinate2D location;
//        location.latitude = aUserLocation.coordinate.latitude;
//        location.longitude = aUserLocation.coordinate.longitude;
//        region.span = span;
//        region.center = location;
//        [aMapView setRegion:region animated:YES];
//        self.didAlreadySetRegion = YES;
//    }
//}


@end

//
//  main.m
//  XModeAPI
//
//  Created by Gabriel Jensen on 12/02/2016.
//  Copyright (c) 2016 Gabriel Jensen. All rights reserved.
//

@import UIKit;
#import "XMDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([XMDAppDelegate class]));
    }
}

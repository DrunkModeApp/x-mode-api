//
//  XMDViewController.m
//  XModeAPI
//
//  Created by Gabriel Jensen on 12/02/2016.
//  Copyright (c) 2016 Gabriel Jensen. All rights reserved.
//

#import "XMDViewController.h"
#import "XModeSdkVisitedPlace.h"
#import "XMDAppDelegate.h"
#import <XModeAPI/XModeAPI.h>
#import "CoreMotion/CoreMotion.h"
#import <AdSupport/ASIdentifierManager.h>
#import "XModeSdkLocationPublisher.h"

@interface XMDViewController ()

@property (nonatomic) CMMotionManager *motionManager;

@end

@implementation XMDViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.pointsLabel.text = @"";
    [self displayNumGeoFence];
    
   // self.backgroundInfoLabel.text = @"total points:";
    self.resetButton.hidden = YES;
    [self displayLocationDate];
    
    [self.viewableSwitch addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
    BOOL isPhoneInViewingPosition =   [[NSUserDefaults standardUserDefaults] boolForKey:@"IsPhoneInViewablePosition"];
    [self.viewableSwitch setOn:isPhoneInViewingPosition animated:NO];
    
    NSString *idfaString = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    self.idfaTextField.text = [NSString stringWithFormat:@"%@", idfaString];
    
    self.motionManager = [[CMMotionManager alloc] init];
    self.motionManager.accelerometerUpdateInterval = .01;
    self.motionManager.gyroUpdateInterval = 1;
    [self.motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue]
                                             withHandler:^(CMAccelerometerData  *accelerometerData, NSError *error) {
                                                 [self outputAccelerationData:accelerometerData.acceleration];
                                                 if(error){
                                                     NSLog(@"%@", error);
                                                 }
                                             }];
    
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(handleCurrentLocationDidChange:) name:@"XModeCurrentLocationDidChangeNotification" object:nil];
    [nc addObserver:self selector:@selector(handleGeoFenceDidExit:) name:@"XModeGeoFenceDidExit" object:nil];
    
    
}

-(void)outputAccelerationData:(CMAcceleration)acceleration
{
    return;
    BOOL isPhoneInViewingPosition = (acceleration.z < 0.5);
    self.view.backgroundColor = isPhoneInViewingPosition ? [UIColor whiteColor] : [UIColor blueColor];
   // NSLog(@"x:%.2f y:%.2f, z:%.2f bright:%.", acceleration.x,acceleration.y,acceleration.z);
    if (acceleration.y > .8) {
        self.view.backgroundColor = [UIColor greenColor];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handleCurrentLocationDidChange:(NSNotification *)notif
{
   // NSDictionary *locationInfo = notif.userInfo;
   // XModeSdkVisitedPlace *vp = locationInfo[XModeCurrentLocationKey];
    [self displayLocationDate];
}

- (void)handleGeoFenceDidExit:(NSNotification *)notif
{
    // NSDictionary *locationInfo = notif.userInfo;
    // XModeSdkVisitedPlace *vp = locationInfo[XModeCurrentLocationKey];
    [self displayNumGeoFence];
}

- (void)switchValueChanged:(UISwitch *)theSwitch
{
    BOOL flag = self.viewableSwitch.isOn;
    [[NSUserDefaults standardUserDefaults] setBool:flag forKey:@"IsPhoneInViewablePosition"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) displayLocationDate {
    
    NSDate *lastDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"LastLocationDate"];
    if (lastDate) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM d, h:mm:ss a"];
        NSString *dateStr = [dateFormat stringFromDate:lastDate];
        self.pointsLabel.text = [NSString stringWithFormat:@"Last while-in-use location:\n%@",dateStr];
    }
    
    NSDate *lastDateBg = [[NSUserDefaults standardUserDefaults] objectForKey:@"LastLocationDateBackground"];
    if (lastDateBg) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM d, h:mm:ss a"];
      //  NSString *dateStr = [dateFormat stringFromDate:lastDateBg];
      //  NSInteger num = [[NSUserDefaults standardUserDefaults] integerForKey:@"NumPointsBackground"];
        
        NSArray *visitedPlaces = [[XModeSdkLocationPublisher sharedInstance] visitedPlacesForUser];
        
        self.backgroundInfoLabel.text = [NSString stringWithFormat:@"Points last 24 hours: %ld",visitedPlaces.count];
    }
    
}

- (void) displayNumGeoFence {
    NSInteger num = [[NSUserDefaults standardUserDefaults] integerForKey:@"NumTimesGeoFenceWake"];
    self.geoFenceLabel.text = @""; //[NSString stringWithFormat:@"Num times wake for geofence:%ld",(long)num];
}

@end

#
# Be sure to run `pod lib lint XModeAPI.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'XModeAPI'
  s.version          = '1.0.14'
  s.summary          = 'Location tracking'

# This description is used to generate tags and improve search results.
#   * Location tracking

  s.description      = <<-DESC
Location-based tracking API for use in apps that have location on all the time.
                       DESC

  s.homepage         = 'https://bitbucket.org/DrunkModeApp/x-mode-api'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'XMode Gabriel Jensen' => 'gabriel@drunkmode.com' }
  s.source           = { :git => 'http://bitbucket.org/DrunkModeApp/x-mode-api', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'
  s.ios.vendored_frameworks = "XModeAPI/AJKit.framework"
  s.source_files = 'XModeAPI/Classes/**/*','XModeAPI/AJKit.framework/Headers/*.h'
 
  # s.resource_bundles = {
  #   'XModeAPI' => ['XModeAPI/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.ios.public_header_files  = 'XModeAPI/AJKit.framework/Versions/A/Headers/*.h'
 
  s.dependency 'AFNetworking'
  s.dependency 'BeaconsInSpace'
end
